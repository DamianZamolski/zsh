export EDITOR=nvim
export HISTFILE=~/.zsh_history
export HISTSIZE=1000
export NODE_EXTRA_CA_CERTS=~/c/certificates/npm-certificate
export PATH=$PATH:~/dz/zsh/bin:~/.local/bin
export SAVEHIST=1000
export _JAVA_AWT_WM_NONREPARENTING=1 # Fix Jetbrains
